See git-pull(1) for details.

    git pull <remote> <branch>

If you wish to set tracking information for this branch you can do so with:

    git branch --set-upstream-to=<remote>/<branch> master


User@DESKTOP-PCS5VNI MINGW64 /c/git (master)
$ git pull https://github.com/Vasantha-gcp/Projectdetails.git
remote: Enumerating objects: 22, done.
remote: Counting objects: 100% (22/22), done.
remote: Compressing objects: 100% (14/14), done.
remote: Total 22 (delta 7), reused 15 (delta 5), pack-reused 0
Unpacking objects: 100% (22/22), 655.38 KiB | 1.55 MiB/s, done.
From https://github.com/Vasantha-gcp/Projectdetails
 * branch            HEAD       -> FETCH_HEAD

User@DESKTOP-PCS5VNI MINGW64 /c/git (master)
$ git add f2 f3

User@DESKTOP-PCS5VNI MINGW64 /c/git (master)
$ git status f2 f3
On branch master
Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
        modified:   f2
        modified:   f3


User@DESKTOP-PCS5VNI MINGW64 /c/git (master)
$ git commit -m "f2f3commit"
Author identity unknown

*** Please tell me who you are.

Run

  git config --global user.email "you@example.com"
  git config --global user.name "Your Name"

to set your account's default identity.
Omit --global to set the identity only in this repository.

fatal: unable to auto-detect email address (got 'User@DESKTOP-PCS5VNI.(none)')

User@DESKTOP-PCS5VNI MINGW64 /c/git (master)
$ gi config --global user.email "nallapareddygcp12@gmail.com"
bash: gi: command not found

User@DESKTOP-PCS5VNI MINGW64 /c/git (master)
$ git config --global user.email "nallapareddygcp12@gmail.com"

User@DESKTOP-PCS5VNI MINGW64 /c/git (master)
$ git config --global user.name "Vasantha-gcp"

User@DESKTOP-PCS5VNI MINGW64 /c/git (master)
$ git commit -m "f2f3commit"
[master b314fb8] f2f3commit
 2 files changed, 5 insertions(+)

User@DESKTOP-PCS5VNI MINGW64 /c/git (master)
$ git push origin master
fatal: 'origin' does not appear to be a git repository
fatal: Could not read from remote repository.

Please make sure you have the correct access rights
and the repository exists.

User@DESKTOP-PCS5VNI MINGW64 /c/git (master)
$ git push origin main
error: src refspec main does not match any
error: failed to push some refs to 'origin'

User@DESKTOP-PCS5VNI MINGW64 /c/git (master)
$ git remote origin^C

User@DESKTOP-PCS5VNI MINGW64 /c/git (master)
$ git remote origin https://github.com/Vasantha-gcp/Projectdetails.git
error: Unknown subcommand: origin
usage: git remote [-v | --verbose]
   or: git remote add [-t <branch>] [-m <master>] [-f] [--tags | --no-tags] [--mirror=<fetch|push>] <name> <url>
   or: git remote rename <old> <new>
   or: git remote remove <name>
   or: git remote set-head <name> (-a | --auto | -d | --delete | <branch>)
   or: git remote [-v | --verbose] show [-n] <name>
   or: git remote prune [-n | --dry-run] <name>
   or: git remote [-v | --verbose] update [-p | --prune] [(<group> | <remote>)...]
   or: git remote set-branches [--add] <name> <branch>...
   or: git remote get-url [--push] [--all] <name>
   or: git remote set-url [--push] <name> <newurl> [<oldurl>]
   or: git remote set-url --add <name> <newurl>
   or: git remote set-url --delete <name> <url>

    -v, --verbose         be verbose; must be placed before a subcommand


User@DESKTOP-PCS5VNI MINGW64 /c/git (master)
$ git remote add origin https://github.com/Vasantha-gcp/Projectdetails.git

User@DESKTOP-PCS5VNI MINGW64 /c/git (master)
$ git push origin main
error: src refspec main does not match any
error: failed to push some refs to 'https://github.com/Vasantha-gcp/Projectdetails.git'

User@DESKTOP-PCS5VNI MINGW64 /c/git (master)
$ git push origin master
Enumerating objects: 7, done.
Counting objects: 100% (7/7), done.
Delta compression using up to 8 threads
Compressing objects: 100% (3/3), done.
Writing objects: 100% (4/4), 372 bytes | 93.00 KiB/s, done.
Total 4 (delta 1), reused 0 (delta 0), pack-reused 0
remote: Resolving deltas: 100% (1/1), completed with 1 local object.
remote:
remote: Create a pull request for 'master' on GitHub by visiting:
remote:      https://github.com/Vasantha-gcp/Projectdetails/pull/new/master
remote:
To https://github.com/Vasantha-gcp/Projectdetails.git
 * [new branch]      master -> master

User@DESKTOP-PCS5VNI MINGW64 /c/git (master)
$ ls
 IMG-20210412-WA0001.jpg                          f2         v1
'NDA trial-Ayush Gupta.pdf'                       f3         v2
 NDA-Danish.pdf                                   f4         v3
'WhatsApp Image 2022-05-04 at 10.23.14 AM.jpeg'   fi         v4
'WhatsApp Image 2022-05-04 at 10.28.34 AM.jpeg'   filetest   v5

User@DESKTOP-PCS5VNI MINGW64 /c/git (master)
$ git add .

User@DESKTOP-PCS5VNI MINGW64 /c/git (master)
$ git commit -m "f3edit"
[master 3b9f647] f3edit
 2 files changed, 7 insertions(+), 1 deletion(-)
 create mode 100644 filetest

User@DESKTOP-PCS5VNI MINGW64 /c/git (master)
$ git push origin main
error: src refspec main does not match any
error: failed to push some refs to 'https://github.com/Vasantha-gcp/Projectdetails.git'

User@DESKTOP-PCS5VNI MINGW64 /c/git (master)
$ git push origin master
Enumerating objects: 5, done.
Counting objects: 100% (5/5), done.
Delta compression using up to 8 threads
Compressing objects: 100% (2/2), done.
Writing objects: 100% (3/3), 315 bytes | 315.00 KiB/s, done.
Total 3 (delta 1), reused 0 (delta 0), pack-reused 0
remote: Resolving deltas: 100% (1/1), completed with 1 local object.
To https://github.com/Vasantha-gcp/Projectdetails.git
   b314fb8..3b9f647  master -> master

User@DESKTOP-PCS5VNI MINGW64 /c/git (master)
$ git status
On branch master
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
        modified:   f3

no changes added to commit (use "git add" and/or "git commit -a")

User@DESKTOP-PCS5VNI MINGW64 /c/git (master)
$ git add f3

User@DESKTOP-PCS5VNI MINGW64 /c/git (master)
$ git commit -m "f33commit"
[master ac2d0c8] f33commit
 1 file changed, 4 insertions(+), 1 deletion(-)

User@DESKTOP-PCS5VNI MINGW64 /c/git (master)
$ git push origin master
Enumerating objects: 5, done.
Counting objects: 100% (5/5), done.
Delta compression using up to 8 threads
Compressing objects: 100% (3/3), done.
Writing objects: 100% (3/3), 302 bytes | 60.00 KiB/s, done.
Total 3 (delta 1), reused 0 (delta 0), pack-reused 0
remote: Resolving deltas: 100% (1/1), completed with 1 local object.
To https://github.com/Vasantha-gcp/Projectdetails.git
   3b9f647..ac2d0c8  master -> master

User@DESKTOP-PCS5VNI MINGW64 /c/git (master)
$ ls
 IMG-20210412-WA0001.jpg                          f2         v1
'NDA trial-Ayush Gupta.pdf'                       f3         v2
 NDA-Danish.pdf                                   f4         v3
'WhatsApp Image 2022-05-04 at 10.23.14 AM.jpeg'   fi         v4
'WhatsApp Image 2022-05-04 at 10.28.34 AM.jpeg'   filetest   v5

User@DESKTOP-PCS5VNI MINGW64 /c/git (master)
$ ls
 IMG-20210412-WA0001.jpg                          f3              v2
'NDA trial-Ayush Gupta.pdf'                       f4              v3
 NDA-Danish.pdf                                   fi              v4
'WhatsApp Image 2022-05-04 at 10.23.14 AM.jpeg'   filetest        v5
'WhatsApp Image 2022-05-04 at 10.28.34 AM.jpeg'   testfilee.txt
 f2                                               v1

User@DESKTOP-PCS5VNI MINGW64 /c/git (master)
$ git status testfilee.txt
On branch master
Untracked files:
  (use "git add <file>..." to include in what will be committed)
        testfilee.txt

nothing added to commit but untracked files present (use "git add" to track)

User@DESKTOP-PCS5VNI MINGW64 /c/git (master)
$ git add testfilee.txt

User@DESKTOP-PCS5VNI MINGW64 /c/git (master)
$ git commit -m "testfile"
[master 67f454c] testfile
 1 file changed, 1 insertion(+)
 create mode 100644 testfilee.txt

User@DESKTOP-PCS5VNI MINGW64 /c/git (master)
$ git push origin master
Enumerating objects: 4, done.
Counting objects: 100% (4/4), done.
Delta compression using up to 8 threads
Compressing objects: 100% (2/2), done.
Writing objects: 100% (3/3), 295 bytes | 295.00 KiB/s, done.
Total 3 (delta 1), reused 0 (delta 0), pack-reused 0
remote: Resolving deltas: 100% (1/1), completed with 1 local object.
To https://github.com/Vasantha-gcp/Projectdetails.git
   ac2d0c8..67f454c  master -> master

User@DESKTOP-PCS5VNI MINGW64 /c/git (master)
$ git push origin main
error: src refspec main does not match any
error: failed to push some refs to 'https://github.com/Vasantha-gcp/Projectdetails.git'

User@DESKTOP-PCS5VNI MINGW64 /c/git (master)
$ git branch
* master

User@DESKTOP-PCS5VNI MINGW64 /c/git (master)
$ git branch -r
  origin/master

User@DESKTOP-PCS5VNI MINGW64 /c/git (master)
$ git switch -c Vasantha
Switched to a new branch 'Vasantha'

User@DESKTOP-PCS5VNI MINGW64 /c/git (Vasantha)
$ git branch
* Vasantha
  master

User@DESKTOP-PCS5VNI MINGW64 /c/git (Vasantha)
$ ls
 IMG-20210412-WA0001.jpg                          f3              v2
'NDA trial-Ayush Gupta.pdf'                       f4              v3
 NDA-Danish.pdf                                   fi              v4
'WhatsApp Image 2022-05-04 at 10.23.14 AM.jpeg'   filetest        v5
'WhatsApp Image 2022-05-04 at 10.28.34 AM.jpeg'   testfilee.txt
 f2                                               v1

User@DESKTOP-PCS5VNI MINGW64 /c/git (Vasantha)
$ git add f2

User@DESKTOP-PCS5VNI MINGW64 /c/git (Vasantha)
$ git commit -m "f2commit vasantha"
On branch Vasantha
nothing to commit, working tree clean

User@DESKTOP-PCS5VNI MINGW64 /c/git (Vasantha)
$ git push origin Vasantha
Total 0 (delta 0), reused 0 (delta 0), pack-reused 0
To https://github.com/Vasantha-gcp/Projectdetails.git
   d877dbd..67f454c  Vasantha -> Vasantha

User@DESKTOP-PCS5VNI MINGW64 /c/git (Vasantha)
$ git status f2
On branch Vasantha
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
        modified:   f2

no changes added to commit (use "git add" and/or "git commit -a")

User@DESKTOP-PCS5VNI MINGW64 /c/git (Vasantha)
$ git add f2

User@DESKTOP-PCS5VNI MINGW64 /c/git (Vasantha)
$ git commit -m "f2modifedcommit vasantha"
[Vasantha 4f7ac34] f2modifedcommit vasantha
 1 file changed, 4 insertions(+)

User@DESKTOP-PCS5VNI MINGW64 /c/git (Vasantha)
$ git push origin Vasantha
Enumerating objects: 5, done.
Counting objects: 100% (5/5), done.
Delta compression using up to 8 threads
Compressing objects: 100% (3/3), done.
Writing objects: 100% (3/3), 324 bytes | 324.00 KiB/s, done.
Total 3 (delta 1), reused 0 (delta 0), pack-reused 0
remote: Resolving deltas: 100% (1/1), completed with 1 local object.
To https://github.com/Vasantha-gcp/Projectdetails.git
   67f454c..4f7ac34  Vasantha -> Vasantha

User@DESKTOP-PCS5VNI MINGW64 /c/git (Vasantha)
$
